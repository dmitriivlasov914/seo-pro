#!/usr/bin/env bash
echo "✨ I solemnly swear not to start anything good"
wp() {
    output=$($wpCLI $1 )

    echo "$output"
}

findRootWp() {
    echo "🔍 Find root folder Wordpress:"

    while [ ! -e wp-config.php ]; do
        if [ $(pwd) = "/" ]; then
            echo " - 🌧️ No WordPress root found" >&2; exit 1
        fi
        cd ../
    done
    
    if [ -e wp-config.php ]; then
        wproot=$(pwd)
        echo " - 📁 $wproot"
    fi
}

initializationWC() {
    ## Checking WP CLI and install WP CLI if will be not found
    echo "🔍 Checking wp-cli:"
    wpCLI=$(which wp)
    if ! hash "$wpCLI" 2> /dev/null;
        then
            echo " - 🌧️ Check failed"
            echo " - 🛠️ Install wp-cli:"
            curl -O -sS https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
            chmod +x wp-cli.phar

            wpCLI="./wp-cli.phar"

            echo "  - 🦠 $($wpCLI --version)"
            echo " - ☀️ Install successful" 
        else
            echo " - 🦠 $($wpCLI --version)"
            echo " - ☀️ Check successful"
    fi
}

initializationPlugin() {
    echo "🛠️ Install plugin"
    wp "plugin install --url https://gitlab.com/dmitriivlasov914/seo-pro/-/archive/main/seo-pro-main.zip"
    echo "🔔 Enable plugin"
    wp "plugin activate seo-pro-main"
}

deinitializationPlugin() {
    echo "🔕 Disable plugin"
    wp "plugin deactivate seo-pro-main"
    echo "🗑️ Delete plugin"
    wp "plugin delete seo-pro-main"
}

clearSteps() {
    echo "🧹 Сleaning steps:"

    dirCache="./wp-content/cache"
    if [ -d "$dirCache" ]; then
        echo " - 🗑️ Delete cache"
        rm -rf $dirCache
    fi

    fileWC="wp-cli.phar"
    if [ -f "$fileWC" ]; then
        echo " - 🗑️ Delete wp-cli.phar"
        rm -rf $fileWC
    fi

    fileInit="./wp-content/plugins/seo-pro-main/init.sh"
    if [ -f "$fileInit" ]; then
        echo " - 🗑️ Delete init.sh"
        rm -rf $fileInit
    fi

    echo " - ☀️ Cleaning successful"

    echo "🪄  Mischief Managed!"
}

findRootWp
initializationWC
case "$1" in
    install)
        initializationPlugin
        ;;
    delete)
        deinitializationPlugin
        ;;
    *)
        echo "unknown options"
        ;;
esac

clearSteps
exit
